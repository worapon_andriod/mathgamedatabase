package buu.worapon.mathgame.login

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import buu.worapon.mathgame.R
import buu.worapon.mathgame.database.Player
import buu.worapon.mathgame.database.PlayerDatabaseDao
import kotlinx.coroutines.launch


class LoginViewModel(private val database: PlayerDatabaseDao, private val application: Application) : ViewModel() {
    val username = MutableLiveData<String>()

    private val _player = MutableLiveData<Player>()
    val player: LiveData<Player>
        get() = _player

    private val _eventLogin = MutableLiveData<Boolean>()
    val eventLogin: LiveData<Boolean>
        get() = _eventLogin

    private val _txtValid = MutableLiveData<String>()
    val txtValid: LiveData<String>
        get() = _txtValid

    init {
        _player.value = Player()
        _eventLogin.value = false
        _txtValid.value = ""

    }

    fun eventLoginComplete() {
        _eventLogin.value = false
    }



    fun onClickedLoginBtn() {
       viewModelScope.launch {
            if (username.value!=null) {
                if (username.value!!.length <= 8){
                    if (!isPlayerExist()) {
                        var newPlayer = Player()
                        newPlayer.setUsername(username.value.toString())!!
                        database.insert(newPlayer)
                    }
                    _eventLogin.value = true
                }else{
                    _txtValid.value = application.getString((R.string.usernameToLongTxt))
                }
            }else {
                _txtValid.value = application.getString(R.string.usernameNullTxt)
            }
        }
    }

    private suspend fun isPlayerExist(): Boolean {
        val player = database.getUsername(username.value.toString())
        return if (player != null) {
            _player.value = player
            true
        } else {
            false
        }
    }

}
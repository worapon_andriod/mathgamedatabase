package buu.worapon.mathgame.login

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.worapon.mathgame.database.PlayerDatabaseDao
import java.lang.IllegalArgumentException

class LoginViewModelFactory (private val dataSource: PlayerDatabaseDao, private  val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
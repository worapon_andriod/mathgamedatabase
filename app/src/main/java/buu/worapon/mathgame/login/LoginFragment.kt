package buu.worapon.mathgame.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.worapon.mathgame.R
import buu.worapon.mathgame.database.PlayerDatabase
import buu.worapon.mathgame.databinding.FragmentLoginBinding
import buu.worapon.mathgame.databinding.FragmentMenuBinding

class LoginFragment :Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModelFactory:  LoginViewModelFactory
    private lateinit var viewModel : LoginViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login,container,false)

        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao
        viewModelFactory = LoginViewModelFactory(dataSource, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        viewModel.eventLogin.observe(viewLifecycleOwner, Observer { eventLogin ->
            if (eventLogin) {
                view?.findNavController()?.navigate(LoginFragmentDirections.actionLoginFragmentToMenuFragment(viewModel.player.value!!.getPlayerId()))
                viewModel.eventLoginComplete()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        binding.loginViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }



}
package buu.worapon.mathgame

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.worapon.mathgame.database.PlayerDatabase
import buu.worapon.mathgame.databinding.FragmentCalculateBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [CalculateFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalculateFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var menu = 0

    private lateinit var binding: FragmentCalculateBinding
    private lateinit var viewModel: CalculateViewModel
    private lateinit var viewModelFactory: CalculateViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calculate, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao


        viewModelFactory = CalculateViewModelFactory(
            dataSource,
            CalculateFragmentArgs.fromBundle(requireArguments()).playerId,
            CalculateFragmentArgs.fromBundle(requireArguments()).menu
        )


        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CalculateViewModel::class.java)
        binding.calculateViewModel = viewModel
        binding.setLifecycleOwner(this)


        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.findNavController()?.navigate(
                CalculateFragmentDirections.actionCalculateFragmentToMenuFragment(
                    viewModel.player.value!!.getPlayerId()
                )
            )
        }

        viewModel.hasClicked.observe(viewLifecycleOwner, Observer { hasClicked ->
            binding.invalidateAll()
            if (hasClicked) {
                onCheckAnswer()
                viewModel.onHasClickedFinished()
            }

        })

        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root

    }


    private fun onCheckAnswer() {
        viewModel.checkResult()
        if (viewModel.resultBoolean.value!!) {
            binding.txtAnswer.setTextColor(Color.parseColor("#00ff00"))
        } else {
            binding.txtAnswer.setTextColor(Color.parseColor("#ff0000"))
        }
    }

}
package buu.worapon.mathgame

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.worapon.mathgame.database.PlayerDatabaseDao

class CalculateViewModelFactory(
    private val dataSource: PlayerDatabaseDao,
    private val playerId: Long,
    private val menu: Int
):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CalculateViewModel::class.java)) {
            return CalculateViewModel(dataSource,playerId,menu) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
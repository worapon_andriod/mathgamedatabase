package buu.worapon.mathgame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.worapon.mathgame.database.PlayerDatabase
import buu.worapon.mathgame.databinding.FragmentMenuBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [MenuFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MenuFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private  var menu =0

    private lateinit var viewModel: MenuViewModel
    private lateinit var viewModelFactory: MenuViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentMenuBinding>(inflater, R.layout.fragment_menu,container,false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao

        viewModelFactory = MenuViewModelFactory(dataSource,MenuFragmentArgs.fromBundle(requireArguments()).playerId)
        viewModel = ViewModelProvider(this,viewModelFactory).get(MenuViewModel::class.java)

        viewModel.eventClickBtnSum.observe(viewLifecycleOwner, Observer { eventClickBtnSum ->
            if(eventClickBtnSum){
                Log.i("result", "0")
                menu = 1
                view?.findNavController()?.navigate(MenuFragmentDirections.actionMainFragmentToCalculateFragment(1,viewModel.player.value!!.getPlayerId()))
                viewModel.onClickBtnSumFinished()
            }
        })
        viewModel.eventClickBtnMinus.observe(viewLifecycleOwner, Observer { eventClickBtnMinus ->
            if(eventClickBtnMinus){
                menu = 2
                view?.findNavController()?.navigate(MenuFragmentDirections.actionMainFragmentToCalculateFragment(menu,viewModel.player.value!!.getPlayerId()))
                viewModel.onClickBtnMinusFinished()
            }
        })
        viewModel.eventClickBtnMulti.observe(viewLifecycleOwner, Observer { eventClickBtnMulti ->
            if(eventClickBtnMulti){
                menu = 3
                view?.findNavController()?.navigate(MenuFragmentDirections.actionMainFragmentToCalculateFragment(menu,viewModel.player.value!!.getPlayerId()))
                viewModel.onClickBtnMultiFinished()
            }

        })
        viewModel.eventClickBtnDivide.observe(viewLifecycleOwner, Observer { eventClickBtnDivide ->
            if(eventClickBtnDivide){
                menu = 4
                view?.findNavController()?.navigate(MenuFragmentDirections.actionMainFragmentToCalculateFragment(menu,viewModel.player.value!!.getPlayerId()))
                viewModel.onClickBtnDivideFinished()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            view?.findNavController()?.navigate(MenuFragmentDirections.actionMenuFragmentToLoginFragment())
        }

        binding.menuViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }


}
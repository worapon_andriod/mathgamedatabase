package buu.worapon.mathgame.rank

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.worapon.mathgame.database.Player
import buu.worapon.mathgame.database.PlayerDatabaseDao

class RankViewModel (private val database: PlayerDatabaseDao): ViewModel(){

    val playerList = database.getScoreCorrect()

}
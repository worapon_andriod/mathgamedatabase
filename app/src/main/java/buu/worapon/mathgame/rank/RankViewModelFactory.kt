package buu.worapon.mathgame.rank

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.worapon.mathgame.CalculateViewModel
import buu.worapon.mathgame.database.PlayerDatabaseDao
import javax.sql.DataSource

class RankViewModelFactory (private val dataSource: PlayerDatabaseDao) :ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RankViewModel::class.java)) {
            return RankViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
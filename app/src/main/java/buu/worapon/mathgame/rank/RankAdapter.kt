package buu.worapon.mathgame.rank

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView
import buu.worapon.mathgame.R
import buu.worapon.mathgame.database.Player

class RankAdapter : RecyclerView.Adapter<RankAdapter.ViewHolder>() {

    var data = listOf<Player>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int ) {
        val item = data[position]
        holder.bind(item,position+1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        private val textRank : TextView = itemView.findViewById(R.id.text_rank)
        private val textUsername : TextView = itemView.findViewById(R.id.text_username)
        private val textScore : TextView = itemView.findViewById(R.id.text_score)


        fun bind(user: Player, rank: Int) {
            textRank.text = rank.toString()
            textUsername.text = user.getUsername()
            textScore.text = user.getScoreCorrect().toString()
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.list_rank, parent, false)
                return ViewHolder(view)
            }
        }

    }



}


package buu.worapon.mathgame.rank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import buu.worapon.mathgame.R
import buu.worapon.mathgame.database.PlayerDatabase
import buu.worapon.mathgame.databinding.FragmentRankBinding

class RankFragment : Fragment() {
    private lateinit var binding : FragmentRankBinding
    private lateinit var viewModel : RankViewModel
    private lateinit var viewModelFactory : RankViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rank, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao

        viewModelFactory = RankViewModelFactory(dataSource)
        viewModel = ViewModelProvider(this,viewModelFactory).get(RankViewModel::class.java)

        val adapter = RankAdapter()

        binding.rankList.adapter = adapter
        binding.rankViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.playerList.observe(viewLifecycleOwner, Observer {
            it?.let{
                adapter.data = it
            }
        })



        return binding.root
    }
}

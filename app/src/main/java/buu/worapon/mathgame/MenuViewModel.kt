package buu.worapon.mathgame

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import buu.worapon.mathgame.database.Player
import buu.worapon.mathgame.database.PlayerDatabaseDao
import kotlinx.coroutines.launch

class MenuViewModel (private val database: PlayerDatabaseDao, playerId: Long): ViewModel() {
    private val playerId: Long = playerId

    private val _player = MutableLiveData<Player>()
    val player: LiveData<Player>
        get() = _player


    private val _eventClickBtnSum = MutableLiveData<Boolean>()
    val eventClickBtnSum : LiveData<Boolean>
        get() = _eventClickBtnSum


    fun onClickBtnSum() {
        _eventClickBtnSum.value = true
    }

    fun onClickBtnSumFinished() {
        _eventClickBtnSum.value = false
    }

    private val _eventClickBtnMinus = MutableLiveData<Boolean>()
    val eventClickBtnMinus : LiveData<Boolean>
        get() = _eventClickBtnMinus


    fun onClickBtnMinus() {
        _eventClickBtnMinus.value = true
    }

    fun onClickBtnMinusFinished() {
        _eventClickBtnMinus.value = false
    }

    private val _eventClickBtnMulti = MutableLiveData<Boolean>()
    val eventClickBtnMulti : LiveData<Boolean>
        get() = _eventClickBtnMulti


    fun onClickBtnMulti() {
        _eventClickBtnMulti.value = true
    }

    fun onClickBtnMultiFinished() {
        _eventClickBtnMulti.value = false
    }

    private val _eventClickBtnDivide = MutableLiveData<Boolean>()
    val eventClickBtnDivide : LiveData<Boolean>
        get() = _eventClickBtnDivide


    fun onClickBtnDivide() {
        _eventClickBtnDivide.value = true
    }

    fun onClickBtnDivideFinished() {
        _eventClickBtnDivide.value = false
    }


    init{
        viewModelScope.launch {
            _player.value = database.get(playerId)
        }
        //_score.value = score
    }

//    private suspend fun getPlayer(id: Long): Player? {
//        return database.get(id)
//    }
}
package buu.worapon.mathgame.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "player_table")

data class Player (
    @PrimaryKey(autoGenerate = true)
    private var playerId : Long = 0L,

    @ColumnInfo(name = "username")
    private var username: String = "",

    @ColumnInfo(name = "score_correct")
    private var scoreCorrect: Int = 0,

    @ColumnInfo(name = "score_incorrect")
    private var scoreIncorrect: Int = 0
){
    fun setPlayerId (id: Long) {
        this.playerId = id
    }

    fun getPlayerId () = this.playerId

    fun setUsername (username: String) {
        this.username = username
    }

    fun getUsername () = this.username

    fun setScoreCorrect(scoreCorrect: Int) {
        this.scoreCorrect =scoreCorrect
    }

    fun getScoreCorrect() = this.scoreCorrect

    fun setScoreIncorrect(scoreIncorrect: Int) {
        this.scoreIncorrect = scoreIncorrect
    }

    fun getScoreIncorrect() = this.scoreIncorrect

    fun plusCorrect(){
        this.scoreCorrect++
    }
    fun plusIncorrect(){
        this.scoreIncorrect++
    }
}
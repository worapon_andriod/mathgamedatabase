package buu.worapon.mathgame.database

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface PlayerDatabaseDao {

    @Insert
    suspend fun insert(player: Player)

    @Update
    suspend fun update(player: Player)

    @Query("SELECT * from player_table where playerId = :key")
    suspend fun  get(key: Long): Player?

    @Query("SELECT * from player_table where username = :name")
    suspend fun getUsername(name: String): Player?

    @Query("SELECT * from player_table ORDER BY score_correct DESC")
    fun getScoreCorrect(): LiveData<List<Player>>

}